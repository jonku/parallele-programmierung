// Reference: http://en.wikibooks.org/wiki/Algorithm_Implementation/Simulation/Conway's_Game_of_Life

//Important: use UTF-8 encoding! (javac -encoding utf8)
import java.io.*;
import java.util.ArrayList;
import java.util.List;
public class GOL extends Thread {
	
	private static int board_width = 0;
	private static int board_height = 0;
	private volatile static boolean[][] board;
	private volatile static boolean threadsRunning = true;
	
	private int worker_board_width = 0;
	private int worker_board_height = 0;
	private int worker_x_start = 0;
	private int worker_y_start = 0;
	private boolean[][] worker_board;
    
    private final Object lock;
    private boolean finished = false;

    public GOL(int xStart, int yStart, int width, int height, final Object lock) {
		worker_board_width = width;
		worker_board_height = height;
		worker_x_start = xStart;
		worker_y_start = yStart;
        this.lock = lock;
	}
	
	public void resetWorker() {
		worker_board = new boolean[worker_board_width][worker_board_height];
		finished = false;
	}
	
	public int getXStart() {
		return worker_x_start;
	}
	
	public int getYStart() {
		return worker_y_start;
	}
	
	public int getWorkerWidth() {
		return worker_board_width;
	}
	
	public int getWorkerHeight() {
		return worker_board_height;
	}
	
	private boolean getWorkerCell(int x, int y) {
		return worker_board[x][y];
	}
	
	public static String boardToString() {

		StringBuffer sb= new StringBuffer();  // used to print out the board at the end
		for (int j=0;j< board_height;j++) {
			for (int i=0;i< board_width;i++) {
				if (board[i][j]) {
//					sb.append('█');
                    sb.append("X");
				} else {
//					sb.append('░');
                    sb.append(".");
				}
			}
		sb.append("\n");
		}
		return sb.toString();
	}
 
	// Converts the string from the input file to a boolean array
	public static boolean[][] stringToBoolArray(String str) {

		boolean[][] board = new boolean[board_width][board_height];
		// Reads per column from top to bottom
		for (int i=0;i< board_width; i++) {
			for (int j=0; j< board_height; j++) {
				board[i][j] = str.charAt((j* board_width)+i) != '0';
			}
		}
		return board;
	}

	// Returns the amount of living neighbour cells
	private static int countNeighbourCells(int x, int y) {
		int count = 0;
		
		//Top
		if(y-1 >= 0)
			if (board[x][y-1])
				count++;
		
		//Bottom
		if(y+1 < board_height)
			if (board[x][y+1])
				count++;
		
		//Right
		if (x+1 < board_width)
			if (board[x+1][y])
				count++;

		//Right Top
		if(x+1 < board_width && y-1 >= 0)
			if (board[x+1][y-1])
				count++;	

		//Right Bottom
		if(x+1 < board_width && y+1 < board_height)
			if (board[x+1][y+1])
				count++;

		//Left
		if(x-1 >= 0)
			if (board[x-1][y])
				count++;
		
		//Left Top
		if(x-1 >= 0 && y-1 >= 0)
			if (board[x-1][y-1])
				count++;
		
		//Left Bottom
		if(x-1 >= 0 && y+1 < board_height)
			if (board[x-1][y+1])
				count++;
		
		return count;
	}


 
	private static boolean calculateCell(int x, int y) {
		// this function applies the rules of the game on one square
		int neighbourCount = countNeighbourCells(x, y);
		if (board[x][y]) {
			//Living Cell
			return (neighbourCount == 2 || neighbourCount == 3);
		} else {
			//Dead Cell
			return (neighbourCount == 3);
		}
	}
	
	// Utility method
	public static int floorDiv(int x, int y) {
		int r = x / y;
		// if the signs are different and modulo not zero, round down
		if ((x ^ y) < 0 && (r * y != x)) {
		    r--;
		}
		return r;
	}
 
    public static void stopThreads() {
        threadsRunning = false;
    }

    public boolean isCalculating() {
        return !finished;
    }

	@Override
	public void run() {
		//Calculate
        while(threadsRunning) {
        	// Horizontal
            for (int x = worker_x_start; x < worker_board_width + worker_x_start; x++) {
            	// Vertical
                for (int y = worker_y_start; y < worker_board_height + worker_y_start; y++) {
                    worker_board[x - worker_x_start][y - worker_y_start] = calculateCell(x, y);
                }
            }
            synchronized (lock) {
                try {
                    finished = true;
                    // Wait for new calculation
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	
	public static void main(String[] args) throws java.io.IOException {
		// Takes board size and number of generations from arguments
		
		// Shared lock object
        Object lock = new Object();
        
        // Board size
		board_width = Integer.parseInt(args[0]);
		board_height = Integer.parseInt(args[1]);
		// Number of generations to calculate
		int generations = Integer.parseInt(args[2]);

		// Read board from file
		FileReader fileReader = new FileReader(args[3]);
		BufferedReader bufferedReader= new BufferedReader(fileReader);
		StringBuilder stringBuilder = new StringBuilder();
		
		// Read first line
		String line = bufferedReader.readLine();
		while(line != null) {
			stringBuilder.append(line);
			// Read next line
			line = bufferedReader.readLine();
		}
		bufferedReader.close();

		String s = stringBuilder.toString();
		// Convert string to bool-array
		board = stringToBoolArray(s);
		
		// Print the selected board filename
		System.out.println(args[3] + "\n");

		// Show the input board
		System.out.println("Generation 0");
		System.out.println(boardToString());
		
		
		ArrayList<GOL> workers = new ArrayList<>();
		
		//Static generation of Threads!
		int xSize1 = floorDiv(board_width, 2);
		int xSize2 = xSize1 + (board_width % 2);
		int ySize1 = floorDiv(board_height, 2);
		int ySize2 = ySize1 + (board_height % 2);

		/* Uncomment the following line and comment the other workers.add lines
		 * to run this program single-threaded
		 */
//		workers.add(new GOL(0, 0, board_width, board_height, lock));
		
		// Adds 4 workers to run this program with 4 threads
		workers.add(new GOL(0, 0, xSize1, ySize1, lock));
		workers.add(new GOL(xSize1, 0, xSize2, ySize1, lock));
		workers.add(new GOL(0, ySize1, xSize1, ySize2, lock));
		workers.add(new GOL(xSize1, ySize1, xSize2, ySize2, lock));
		
		// Measure time
		long startTime = System.currentTimeMillis();
		
		// Calculate all generations
		for (int i=0; i< generations; i++) {
			
			// Reset workers
			for(GOL worker : workers) {
				worker.resetWorker();
                // If first generation, start threads
                if(i == 0) {
                    worker.start();
                }
			}

            // Restart waiting threads
            if(i != 0) {
                synchronized (lock) {
                    lock.notifyAll();
                }
            }

			//Synchronize
			for(GOL worker : workers) {
				while(worker.isCalculating()) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
			//Put worker-boards together
			for(GOL worker : workers) {
				//Boards of the workers start at (0|0)
				for(int x = 0; x < worker.getWorkerWidth(); x++) {
					for(int y = 0; y < worker.getWorkerHeight(); y++) {
						board[x + worker.getXStart()][y + worker.getYStart()] = worker.getWorkerCell(x, y);
					}
				}
			}
			
			// Show the just calculated generation
//			System.out.println("Generation " + (i+1));
//			System.out.println(boardToString());
		}
		// End of calculations, stop threads

        // Stop threads and let the workers leave their while-loop
        stopThreads();

        synchronized (lock) {
        	// Wake up threads, let them leave their while loop and let them stop
            lock.notifyAll();
        }

        // Stop time measure
		long endTime = System.currentTimeMillis();
		long executionTime = endTime - startTime;
		String executionTimeStr = Long.toString(executionTime);

        // Print the final board
        System.out.println("Final board:\n" + boardToString());
        // Print execution time
		System.out.println("Execution time: " + executionTimeStr + "ms");

	}

}