import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class ParallelFactorial extends Thread {	
	
	// Get available Threads
	private static int threads = Runtime.getRuntime().availableProcessors();

	private static long factorial = 0;
	// Start and End for each thread
	private long workerStart = 0; 
	private long workerEnd = 0;
	// Thread Factorial
	private BigInteger workerResult = new BigInteger("1");

	// Final Result
	private static BigInteger result = new BigInteger("1");
	
	
	// floorDiv function from java 8 java.lang.Math.floorDiv rewritten as per:
	// http://stackoverflow.com/questions/27643616/ceil-conterpart-for-math-floordiv-in-java
	
	public static long floorDiv(long x, long y) {
		long r = x / y;
		// if the signs are different and modulo not zero, round down
		if ((x ^ y) < 0 && (r * y != x)) {
		    r--;
		}
		return r;
	}
	
	private ParallelFactorial(){		
	}
	
	// Set max thread-number to different value than processors
	public static void setThreads(int numberOfThreads){
		threads = numberOfThreads;
	}
	
	private long getWorkerStart() {
		return workerStart;
	}
	
	private long getWorkerEnd() {
		return workerEnd;
	}
	
	private static long getNumberOfThreads() {

		if(factorial > threads) {
			return threads;
		} else {
		//	return range
			return factorial;
		}
	}
	
	private BigInteger getResult() {
		return workerResult;
	}
	
	// Set the calculation range for each worker
	private void assignWorkerRange(long start, long end) {
		workerStart = start;
		workerEnd = end;
	}
	
	// Factorial calculation
	@Override
	public void run()
	{
		//Run the (partial) calculation			ToDo: make increase dynamic
		for(long i = getWorkerStart(); i <= getWorkerEnd(); i++) {
			//Factorial-function
			workerResult = workerResult.multiply(BigInteger.valueOf(i));
		}		
	}
	
	// Start calculation
	public static void runCalculation(long factorial) {
		//Calculate the range for each thread
		ParallelFactorial.factorial = factorial;
		long rangePerThread = floorDiv(factorial, getNumberOfThreads());
		long extraRange = factorial % getNumberOfThreads();
		
		//Create ArrayList with Thread-objects
		List<ParallelFactorial> workers = new ArrayList<ParallelFactorial>(); 
	    for(int i = 0; i < getNumberOfThreads(); i++) {
	    	workers.add(new ParallelFactorial());
	    }
		//Assign ranges
	    long startRangeCounter = 1;
	    for(int i = 0; i < getNumberOfThreads(); i++) {
	    	if(i < extraRange) {
	    		workers.get(i).assignWorkerRange(startRangeCounter, startRangeCounter+(rangePerThread));
	    		startRangeCounter += rangePerThread + 1;
	    	} else {
	    		workers.get(i).assignWorkerRange(startRangeCounter, startRangeCounter+(rangePerThread-1));
	    		startRangeCounter += rangePerThread;
	    	}
	    }
		//Start threads
	    long startTime = System.nanoTime();
	    for (ParallelFactorial worker : workers) {
			worker.start();
		}
	    //Wait for threads to terminate
	    for (ParallelFactorial worker : workers) {
	    	try {
				worker.join();
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
	    	
	    	//Gather partial sums
	    	result = result.multiply(worker.getResult());
		}
	    long elapsedTime = (System.nanoTime() - startTime) / 1000000;
		//Output result
		System.out.println("Result: " + result);
		System.out.println("Threads: " + getNumberOfThreads() + ", Time: "+ elapsedTime +"ms");
	}
}
