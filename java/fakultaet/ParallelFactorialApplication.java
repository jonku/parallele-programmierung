import java.util.Scanner;

public class ParallelFactorialApplication {

	public static void main(String[] args) {

		long factorial = 0;
		
		if (args.length > 1) {
			int threads = 1;	
		    try {
		        threads = Integer.parseInt(args[1]);
		    } catch (NumberFormatException e) {
		        System.err.println("Argument" + args[1] + " must be an integer.");
		        System.exit(1);
		    }	
		    ParallelFactorial.setThreads(threads);		    
		}		
		
		if (args.length > 0) {			
		    try {
		        factorial = Long.parseLong(args[0]);
		    } catch (NumberFormatException e) {
		        System.err.println("Argument" + args[0] + " must be a long.");
		        System.exit(1);
		    }		    
		} else {			
			System.out.print("Calculate factorial of: ");
			Scanner in = new Scanner(System.in);
			factorial = in.nextLong();
			in.close();
		}

		ParallelFactorial.runCalculation(factorial);
	}
}