import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class ParallelSum extends Thread {
	
	//	ToDo:
	//	Make steps dynamic
	// 	Allow negative steps
	//	Allow the use of calculations with something like "n-1"
	
	
	//Get available Threads
	private static int threads = Runtime.getRuntime().availableProcessors();
	//Start and End for calculation
	private static BigInteger range = new BigInteger("0");
	//Start and End for each thread
	private BigInteger workerStart = new BigInteger("0");
	private BigInteger workerEnd = new BigInteger("0");
	//Thread Sum
	private BigInteger workerResult = new BigInteger("0");
	//Final Result
	private static BigInteger result = new BigInteger("0");
	
	//Greater-than and Less-than functions to interpret integer return values to boolean when working with BigInteger
	public static boolean greaterThan(BigInteger x, BigInteger y) {
		if(x.compareTo(y) == 1) //First value is bigger than second value
			return true;
		return false;
	}
	
	public static boolean lessThan(BigInteger x, BigInteger y) {
		if(x.compareTo(y) == -1) //Second value is bigger than first value
			return true;
		return false;
	}
	
	//floorDiv function from java 8 java.lang.Math.floorDiv rewritten as per:
	//http://stackoverflow.com/questions/27643616/ceil-conterpart-for-math-floordiv-in-java
	
	public static BigInteger floorDiv(BigInteger x, BigInteger y) {
		BigInteger r = x.divide(y);
		// if the signs are different and modulo not zero, round down
		if (lessThan(x.xor(y), BigInteger.ZERO) && (r.multiply(y) != x)) {
		    r.subtract(BigInteger.ONE);
		}
		return r;
		/*long r = x / y;
		// if the signs are different and modulo not zero, round down
		if ((x ^ y) < 0 && (r * y != x)) {
		    r--;
		} 
		return r;*/
	}
	
	private ParallelSum(){		
	}
	
	
	//Set max thread-number to different value than processors
	public static void setThreads(int value){
		threads = value;
	}
	
	private BigInteger getWorkerStart() {
		return workerStart;
	}
	
	private BigInteger getWorkerEnd() {
		return workerEnd;
	}
	private static int getNumberOfThreads() {
		if(greaterThan(range, BigInteger.valueOf(threads))) {
			return threads;
		} else {
			return range.intValue();
		}
	}
	
	private BigInteger getResult() {
		return workerResult;
	}
	
	//Set the calculation range for each thread
	private void assignWorkerRange(BigInteger start, BigInteger end) {
		workerStart = start;
		workerEnd = end;
	}
	
	//Sum Algorithm
	@Override
	public void run()
	{
		//Run the partial calculation
		for(BigInteger i = getWorkerStart();
				lessThan(i, getWorkerEnd().add(BigInteger.ONE));
				i=i.add(BigInteger.ONE)) {
			//Sum-function
			workerResult = workerResult.add(i);
		}		
	}
	
	// Start calculation
	public static void runCalculation(BigInteger start, BigInteger end) {
		// Calculate the range for each worker
		range = end.subtract(start).add(BigInteger.ONE);
		BigInteger rangePerWorker = floorDiv(range, BigInteger.valueOf(getNumberOfThreads()));
		BigInteger extraRange = range.mod(BigInteger.valueOf(getNumberOfThreads()));
		
		// Create ArrayList with ParallelSum-objects
		List<ParallelSum> workers = new ArrayList<ParallelSum>(); 
	    for(BigInteger i = BigInteger.valueOf(0);
	    		lessThan(i, BigInteger.valueOf(getNumberOfThreads()));
	    		i = i.add(BigInteger.ONE)) {
	    	workers.add(new ParallelSum());
	    }
		
	    // Assign ranges
	    BigInteger startRangeCounter = start;
	    for(BigInteger i = BigInteger.valueOf(0); 
	    		lessThan(i, BigInteger.valueOf(getNumberOfThreads()));
	    		i=i.add(BigInteger.ONE)) {
	    	if(lessThan(i, extraRange)) // i < extraRange 
	    	{
	    		workers.get(i.intValue()).assignWorkerRange(startRangeCounter, startRangeCounter.add(rangePerWorker));
	    		startRangeCounter = startRangeCounter.add(rangePerWorker.add(BigInteger.ONE)); // startRangeCounter+=rangePerThread+1
	    	} else {
	    		workers.get(i.intValue()).assignWorkerRange(startRangeCounter, startRangeCounter.add(rangePerWorker.subtract(BigInteger.ONE)));
	    		startRangeCounter = startRangeCounter.add(rangePerWorker); //startRangeCounter+=rangePerThread
	    	}
	    }
		
	    // Start threads
	    long startTime = System.nanoTime();
	    for (ParallelSum worker : workers) {
			worker.start();		
	    }
	    
	    // Wait for threads to terminate
	    for (ParallelSum worker : workers) {
	    	try {
				worker.join();
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
	    	// Gather partial sums
	    	result = result.add(worker.getResult());
		}
	    long elapsedTime = (System.nanoTime() - startTime) / 1000000;
		// Output result
		System.out.println("Result: " + result);
		System.out.println("Threads: " + getNumberOfThreads() + ", Time: "+ elapsedTime +"ms");
	}
}
