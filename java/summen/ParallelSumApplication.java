import java.math.BigInteger;
import java.util.Scanner;

public class ParallelSumApplication {

	public static void main(String[] args) {
		if (args.length > 2) {
			
			int threading_level = 1;	
		    try {
		        threading_level = Integer.parseInt(args[2]);
		    } catch (NumberFormatException e) {
		        System.err.println("Argument" + args[2] + " must be an integer.");
		        System.exit(1);
		    }	

		    ParallelSum.setThreads(threading_level);
		    
		} 
		
		
		BigInteger start = BigInteger.valueOf(0), end = BigInteger.valueOf(0);
		
		if (args.length > 1) {
			
		    try {
		        start = new BigInteger(args[0]);
		        end = new BigInteger(args[1]);
		    } catch (NumberFormatException e) {
		        System.err.println("Argument" + args[0] + " and " + args[1] + " must be a long.");
		        System.exit(1);
		    }
		    
		} else {	
			
			System.out.print("Start: ");
			Scanner in = new Scanner(System.in);
			start = in.nextBigInteger();
			System.out.print("End: ");
			end = in.nextBigInteger();
			in.close();            
		
		}
		
		ParallelSum.runCalculation(start, end);
	}
}