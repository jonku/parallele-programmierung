#include <stdio.h>
#include "mpi.h"

int main(int argc, char *argv[]){
    int Hello_World_Rank;
    int Hello_World_Size;
    MPI_Init(&argc,&argv); // MPI initialisieren 
    MPI_Comm_rank(MPI_COMM_WORLD, &Hello_World_Rank); // Prozess ID ermitteln
    MPI_Comm_size(MPI_COMM_WORLD, &Hello_World_Size); // Anzahl der Prozesse ermitteln
    int hello;
	// Nachricht von Prozess 0 an Prozess 1 senden
    if (Hello_World_Rank == 0){
		hello = 33;
		MPI_Send(&hello, 1, MPI_INT, 1, 0, MPI_COMM_WORLD); //Nummer senden
    }
	// Nachricht von Prozess 1 empfangen
	else if (Hello_World_Rank == 1) {
		MPI_Recv(&hello, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); //Nummer empfangen
		printf("Prozess %d hat die Nummer '%d' vom Prozess 0 erhalten.\n",Hello_World_Rank, hello);
    }
	MPI_Finalize();//MPI beenden

    return 0;
}
