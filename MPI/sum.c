
#include <stdio.h>
#include "mpi.h"

int main(int argc, char *argv[]){
    int myRank;
    int size;
    long border_values[2];
    long l_process,u_process;
    int i;
    long double local_result = 0.0;
    long double total;
	// MPI initialisieren
    MPI_Init(&argc,&argv);
    // Rank und Anzahl der Prozesse ermitteln
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* Eingabe der unteren und oberen Grenze durch 
	den Nutzer (an den Prozess 0)*/
    if(myRank==0){
	printf("Geben Sie die untere Grenze an:");
	scanf("%ld",&border_values[0]);
	printf("Geben Sie die obere Grenze an:");
	scanf("%ld", &border_values[1]);
    }
    /* Versenden der Eingabewerte an alle anderen
	Prozesse mittels MPI_Bcast*/
    MPI_Bcast(&border_values, //Eingabeparameter
	      2,     //Anzahl versendeter Elemente
	      MPI_LONG, //Datentyp
	      0,       //Quelle (Prozess 0)
	      MPI_COMM_WORLD); //Kommunikator

    // Berechnen der "lokalen" Unter- und Obergrenzen
    if(myRank==0){
		l_process = border_values[0];
    }else
		l_process = (myRank) * (border_values[1] / size) + 1;
    if(myRank==(size-1))
		u_process = border_values[1];
    else
		u_process = (myRank + 1) * (border_values[1] / size);
    
	// Berechnen der lokalen Summen
    for(i=l_process;i<=u_process;i++){
		local_result = local_result + (double)i;
    }

    /* Zusammenführen aller lokalen Summe zur 
	Gesamtsumme mittels MPI_Reduce*/
    MPI_Reduce(&local_result, /* Operant */
	       &total,        /* Ergebnis */
	       1,             /* Anzahl übergebener Werte */
	       MPI_LONG_DOUBLE,    /* Datentyp */
	       MPI_SUM,      /* Operator */
	       0,             /* Prozess, an den das Ergebnis geschickt wird */
	       MPI_COMM_WORLD); /* Kommunikator */

    // Ausgabe in der Konsole
    if(myRank==0){
		printf("The sum between %ld and %ld is %Lf, and was calculated using %d processes\n",border_values[0], border_values[1],total,size);   
}

    // MPI beenden
    MPI_Finalize();

    return 0;
}
