// Load CodeMirror
window.onload = function() {
	initCodeMirror();
	initSession();
	downloadExampleList();
};

// Load the Codemirror editor
function initCodeMirror() {
	window.editor = CodeMirror.fromTextArea(document.getElementById("code"), {
		mode: "text/x-java",
		lineNumbers: true,
		lineWrapping: true,
		matchBrackets: true,
		foldGutter: {
			rangeFinder: new CodeMirror.fold.combine(CodeMirror.fold.brace, CodeMirror.fold.comment)
		},
		gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
	});
}

function isKeyAvailable() {
	if (typeof key === 'undefined' || key === null) {
		return false;
	}
	return true;
}

// Init the whole session
function initSession() {

	// Get session key
	var sessionCookie = getCookie("parallelProgEditorSession");
	if(sessionCookie == "") {
		generateKey();
	} else {
		key = sessionCookie;
		console.log("Resumed session: " + key);
	}
	
	// Initialize local array for file contents
	fileArray = [];
	fileNameArray = [];
	fileSelected = -1;
	fileExplorerElements = 0;
	
	
	readFileNamesFromCookie();
	// Try to load first file
	loadFile(0);
}

function addFileElement() {
	fileArray[fileExplorerElements] = "";
	var newElement = "<li class=\"fileButton\"><a href=\"#\" id=\"fileLabel_"+fileExplorerElements+"\" onclick=\"loadFile("+fileExplorerElements+")\"></a> <img class=\"edit-button\" src=\"img/pencil.png\" onclick=\"rename("+fileExplorerElements+")\"><img class=\"save-button\" src=\"img/save.png\" onclick=\"addFile("+fileExplorerElements+")\"></li>";
	var newElementMobile = "<li><a href=\"#\" id=\"fileLabelMobile_"+fileExplorerElements+"\" onclick=\"loadFile("+fileExplorerElements+")\"></a></li>";
	// Desktop Menu
	var list = document.getElementById("fileBrowserList");
	list.innerHTML += newElement;
	// Mobile Menu
	var listMobile = document.getElementById("MenuLeft");
	listMobile.innerHTML += newElementMobile;
	fileExplorerElements += 1;
}

function saveFileNamesToCookie() {
	var index;
	var fileNamesString = "";
	var fileName;
	for (index = 0; index < fileNameArray.length; ++index) {
		// Get filename
		fileName = fileNameArray[index];
		// Add to String with "|" character to split entries
		fileNamesString = fileNamesString.concat(fileName);		
		fileNamesString = fileNamesString.concat("|");
	}
	// Remove last "|" character
	fileNamesString = fileNamesString.slice(0, -1);
	setCookie("sessionFileNames", fileNamesString, 1);
}

function readFileNamesFromCookie() {
	var fileNamesString = getCookie("sessionFileNames");
	fileNameArray = fileNamesString.split("|");
	var index;
	
	for(index = 0; index < fileNameArray.length; index++) {
		downloadFile(index);
	}
}

function downloadFile(fileIndex) {
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			// Add Element to FileExplorer
			addFileElement();
			// Read file content to local array
			fileArray[fileIndex] = xhttp.responseText;
			// Set File explorer filenames
			var labelId = "fileLabel_" + fileIndex;
			var labelIdMobile = "fileLabelMobile_" + fileIndex;
			document.getElementById(labelId).innerHTML = fileNameArray[fileIndex];
			document.getElementById(labelIdMobile).innerHTML = fileNameArray[fileIndex];
		}
	}
	var fileToDownload = fileNameArray[fileIndex];
	if(fileToDownload == "") {
		return;
	}
	xhttp.open("POST", "scripts/cat.cgi?KEY="+key+"&FILENAME="+fileNameArray[fileIndex], false);
	xhttp.send();
}

// Generate a new session key and create a folder on the remote server
function generateKey() {

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			key = xhttp.responseText;
			setCookie("parallelProgEditorSession", key, 1);
			console.log("New session key received: " + key);
		}
	}
	xhttp.open("POST", "scripts/init.cgi", false);
	xhttp.send();
}

// Uploads a new file to the remote server
function addFile(fileNumber) {
	// Check key
	if (!isKeyAvailable()) {
		alert("Kein Schlüssel generiert, probier die Datei nochmal zu speichern!");
		generateKey();
		return;
	}
	// Check if filename is set
	var fileNameFromArray = fileNameArray[fileNumber];
	if(typeof fileNameFromArray === 'undefined' || fileNameFromArray === null || fileNameFromArray == "") {
		alert("Kein Dateiname angegeben!");
		return;
	}
	// Get current editor content, if it is the selected file
	if(fileSelected == fileNumber) {
		fileArray[fileNumber] = editor.getValue();
	}	

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			alert("Datei gespeichert!");
		}
	}
	xhttp.open("POST", "scripts/addfile.cgi?KEY="+key+"&FILENAME="+fileNameFromArray, true);
	xhttp.send(fileArray[fileNumber]);
}

function rename(fileNumber) {
	var fileName = fileNameArray[fileNumber];
	var newFileName = prompt("Neuen Dateinamen eingeben:", fileName);
	if (newFileName === null) {
        return; // User pressed cancel!
    }
	// Remove file from server
	removeFileFromServer(fileNumber);
	// Internal rename
	fileNameArray[fileNumber] = newFileName;
	// Reupload file with new name
	addFile(fileNumber);
	var labelId = "fileLabel_" + fileNumber;	
	var labelIdMobile = "fileLabelMobile_" + fileNumber;
	document.getElementById(labelId).innerHTML = newFileName;	
	document.getElementById(labelIdMobile).innerHTML = newFileName;
	
	saveFileNamesToCookie();
}

function removeFileFromServer(fileNumber) {	
	var fileName = fileNameArray[fileNumber];
	if(fileName == "") {
		return;
	}
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			
		}
	}
	xhttp.open("POST", "scripts/rm.cgi?KEY="+key+"&FILENAME="+fileName, true);
	xhttp.send();
}

function loadFile(fileNumber) {

	// Save current editor content
	var oldEditorContent = editor.getValue();
	// Load local file
	var loadedFile = fileArray[fileNumber];
	// Check if file is not empty
	if(typeof loadedFile === 'undefined' || loadedFile === null) {
		return;
	}
	editor.setValue(loadedFile);
	// save old editor content
	if(fileSelected != -1) {
		fileArray[fileSelected] = oldEditorContent;
	}
	// Save the current file selection index
	fileSelected = fileNumber;
}

function getServerStats() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			// Read the response and parse it
			parseServerStats(xhttp.responseText);
		}
	}
	xhttp.open("POST", "scripts/stats.cgi", true);
	xhttp.send();
}

function parseServerStats(statsString) {
	var statsArray = statsString.split("\n");	
	
	// Scan for CPU stats
	var index;
	var regexCPU = /cpu([0-9]+): ([0-9]{0,3}\.[0-9]+%)/g;
	var cpuStatsArray = [];
	var xAxisCategories = [];
	var cpuIndex = 1;
	
	for(index = 0; index < statsArray.length; index++) {
		
		var m;		
		do {
			// Run regex
			m = regexCPU.exec(statsArray[index]);
			if (m) {				
				cpuStatsArray[m[1]] = ["Kern "+cpuIndex, Math.round(parseFloat(m[2]) * 10) / 10];
				xAxisCategories[m[1]] = "Kern "+cpuIndex;
				cpuIndex++;
			}
		} while (m);
	}
	// Get Highcharts object and set data
	var chart = $('#container2').highcharts();
	chart.series[0].setData(cpuStatsArray);	
	chart.xAxis[0].setCategories(xAxisCategories);
	
	// Scan for RAM stats
	var regexRAMFree = /MemFree:\s+([0-9]+)MB/g;
	var ramFree;
	
	for(index = 0; index < statsArray.length; index++) {
		// Run regex
		m = regexRAMFree.exec(statsArray[index]);
		if (m) {
			ramFree = parseInt(m[1]);
			break;
		}
	}
	
	
	var regexRAMTotal = /MemTotal:\s+([0-9]+)MB/g;
	var ramTotal;
	
	for(index = 0; index < statsArray.length; index++) {
		// Run regex
		m = regexRAMTotal.exec(statsArray[index]);
		if (m) {
			ramTotal = parseInt(m[1]);
			break;
		}
	}
	var ramUsed = ramTotal - ramFree;	
	// Get Highcharts object and set data
	var chart = $('#container').highcharts();
	chart.series[0].setData([['Belegt', ramUsed], ['Frei', ramFree]]);
}

function getOutput() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			var output = xhttp.responseText;
			// Put application output in output-box
			document.getElementById("outputbox").innerHTML = output;
		}
	}
	xhttp.open("POST", "scripts/out.cgi?KEY="+key, true);
	xhttp.send();
}

function buildAndRun() {
	
	// Build
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			if(xhttp.responseText != "") {
				window.alert(xhttp.responseText);
			} else {
				// Execute
				run();
			}
		}
	}
	xhttp.open("POST", "scripts/build.cgi?KEY="+key, true);
	xhttp.send();
}

function run() {
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			
		}
		console.log(xhttp.responseText);
	}
	var params = document.getElementById("parameter").value;
	xhttp.open("POST", "scripts/execute.cgi?KEY="+key+"&PARAMS="+params, true);
	xhttp.send();
}

function downloadExampleList() {
	// Get file list of examples from the server
	exampleFileListArray = [];
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			// Put filepaths in an array
			exampleFileListArray = xhttp.responseText.split("\n");
			var index;
			for(index=0; index<exampleFileListArray.length; index++) {
				var file = exampleFileListArray[index];
				// Strip the path-part of the file
				var fileNameStripped = getFileName(file);
				var newElement = "<li><a href=\"#\" class=\"dropentry\" onclick=\"addExampleToSession("+index+")\">"+fileNameStripped+"</a></li>";
				// Desktop menu
				var list = document.getElementById("droplist");
				list.innerHTML += newElement;
			}
		}
	}
	xhttp.open("POST", "scripts/ls.cgi?KEY=beispiele&DIRNAME=", true);
	xhttp.send();
}

function addExampleToSession(exampleFileIndex) {
	// Get file path of the requested file
	var fileName = exampleFileListArray[exampleFileIndex];
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			// Download			
			var fileContent = xhttp.responseText;
			// Add element to filebrowser
			addFileElement();
			var fileIndex = fileExplorerElements -1;
			var labelId = "fileLabel_" + fileIndex;	
			var labelIdMobile = "fileLabelMobile_" + fileIndex;
			var fileNameStripped = getFileName(fileName);
			document.getElementById(labelId).innerHTML = fileNameStripped;	
			document.getElementById(labelIdMobile).innerHTML = fileNameStripped;
			// Put file content into fileArray
			fileArray[fileIndex] = fileContent;
			fileNameArray[fileIndex] = fileNameStripped;
			// Upload to session directory
			addFile(fileIndex);
			
			saveFileNamesToCookie();
		}
	}
	xhttp.open("POST", "scripts/cat.cgi?KEY=beispiele&FILENAME="+fileName, true);
	xhttp.send();
}

// Strips the filepath from the filename
function getFileName(filePath) {
	var slashIndex = filePath.lastIndexOf("/");
	return filePath.substr(slashIndex+1);
}


window.setInterval(function(){
  getServerStats();
  getOutput();
}, 1000);


// Cookie functions by http://www.w3schools.com/js/js_cookies.asp

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}
