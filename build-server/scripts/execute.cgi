#!/bin/bash
# execute.cgi - start the built Project 
# QUERY_STING="?KEY="
# 2016 Jonathan Kuhse

source ./config.cgi
source ./parse_query.cgi

unset REQUEST_METHOD  ## just in case

echo -e "Content-type: text/plain\n"

# Parse QUERY_STRING to variables
parse_query KEY PARAMS

cd $BUILD_DIR/$KEY || exit 2
test "${PWD##$BUILD_DIR}" != "${PWD}" || exit 4


# Execute generated executable script
./execute $PARAMS > .pid 2> .out &
