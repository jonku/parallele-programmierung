#!/bin/bash
# mkdir.cgi - create a new directory DIRNAME
# QUERY_STING="?KEY=&DIRNAME="
# 2016 Jonathan Kuhse

source ./config.cgi
source ./parse_query.cgi

unset REQUEST_METHOD  ## just in case

# Parse QUERY_STRING to variables
parse_query KEY DIRNAME

echo -e "Content-Type: text/plain\n"

cd $BUILD_DIR/$KEY || exit 2
test "${PWD##$BUILD_DIR}" != "${PWD}" || exit 4

# check for wrong relative paths:
#echo $DIRNAME | grep ".." && exit 2
#echo $DIRNAME | grep "^/" && exit 3

if [ -d $DIRNAME ]
then
  # Directory already exists
  exit 0
fi

mkdir $DIRNAME || exit 1
exit 0
