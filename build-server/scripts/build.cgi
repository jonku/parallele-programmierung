#!/bin/bash
# build.cgi - bild the KEY Project
# QUERY_STING="?KEY="
# 2016 Jonathan Kuhse

source ./config.cgi
source ./parse_query.cgi

unset REQUEST_METHOD  ## just in case

# Parse QUERY_STRING to variables
parse_query KEY

cd $BUILD_DIR/$KEY || exit 1
test "${PWD##$BUILD_DIR}" != "${PWD}" || exit 4

echo -e "Content-Type: text/plain\n"


$JAVAC *.java 2>&1 || exit 2
class=$(grep -Rl "public static void main" | sed "s/.java//")

if [ -z "$class" ]
then
   echo "Error: No Main Class found"
fi

if [ ! "$(echo $class | wc -w)"="1" ]
then
   echo "Error: Multiple Main found" || exit 5
fi


# generate an executable script
/usr/bin/env echo -e "\
#!/bin/bash
$JAVA $class \$@ > .out 2> .out &
echo \$!
" > ./execute || exit 3

chmod +x execute || exit 4
