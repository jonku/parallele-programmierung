#!/bin/bash
# stats.cgi - show serverstats
# QUERY_STING="?KEY=&FILENAME="
# 2016 Jonathan Kuhse

source ./config.cgi
source ./parse_query.cgi

unset REQUEST_METHOD

parse_query KEY

echo -e "Content-Type: text/plain\n"

#KEY=$1

cd $BUILD_DIR/$KEY || exit 2
test "${PWD##$BUILD_DIR}" != "${PWD}" || exit 4

PID=$(cat .pid)

if [ -e /proc/$PID/status ]
then
  echo "Status: running" | tr "\t" " " | tr -s " "
  cat /proc/$PID/status > .stat
else
  echo "Status: terminated" | tr "\t" " " | tr -s " "
fi

grep -i vmPeak .stat 						| tr "\t" " " | tr -s " "
grep -i vmSize .stat 						| tr "\t" " " | tr -s " "
grep -i vmSwap .stat 						| tr "\t" " " | tr -s " "
grep -i Threads .stat 						| tr "\t" " " | tr -s " "
grep -i ctxt .stat 						| tr "\t" " " | tr -s " "

echo "MemTotal:  $(free -m | grep Mem | awk '{ print $2 }')MB"
echo   "MemFree: $(free -m | grep Mem | awk '{ print $4 }')MB" 

#eshow cpu-usage with top
export HOME="$(cd ~ && pwd)"
top -bn2d0.4 | grep -ie ^%cpu[0-9] | tail -n $(top -bn1d0.1 | grep -i ^%cpu | wc -l) | tr "/" " " | tr -d ":" | tr -d "%" | sed 's!Cpu!cpu!g' | awk '{ print $1 ": " $2"%" }'
