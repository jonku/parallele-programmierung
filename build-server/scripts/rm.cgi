#!/bin/bash
# rm.cgi - deletes file or directory FILENAME
# QUERY_STING="?KEY=&FILENAME="
# 2016 Jonathan Kuhse

source ./config.cgi
source ./parse_query.cgi

unset REQUEST_METHOD  ## just in case

# Parse QUERY_STRING to variables
parse_query KEY FILENAME


cd $BUILD_DIR/$KEY || exit 2
test "${PWD##$BUILD_DIR}" != "${PWD}" || exit 4

# check if file exists
test -e "$FILENAME" || exit 1

# check for wrong relative paths:
echo "$FILENAME" | grep -F ".." && exit 2
echo "$FILENAME" | grep "^/" && exit 3

rm -rf "$FILENAME" || exit 3

echo -e "Content-Type: text/plain\n"
