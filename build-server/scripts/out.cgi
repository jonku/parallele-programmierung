#!/bin/bash
# out.cgi - show stdout of executed project KEY
# QUERY_STING="?KEY="
# 2016 Jonathan Kuhse

source ./config.cgi
source ./parse_query.cgi

unset REQUEST_METHOD

parse_query KEY

echo -e "Content-Type: text/plain\n"

#KEY=$1

cd $BUILD_DIR/$KEY || exit 2
test "${PWD##$BUILD_DIR}" != "${PWD}" || exit 4

cat .out

