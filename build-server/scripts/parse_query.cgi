# 
# Copyright 2007  Chris F.A. Johnson
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see .

parse_query() #@ Parse parameters passed to a web page
{             #@ USAGE: parse_query VAR ...
    local var val
    local IFS='&'
    unset $*
    vars="&$*&"
    [ "$REQUEST_METHOD" = "POST" ] && read QUERY_STRING
    set -f
    for item in $QUERY_STRING
    do
      var=${item%%=*}
      val=${item#*=}
      val=${val//+/ }
      case $vars in
	     *"&$var&"* )
           case $val in
             *%[0-9a-fA-F][0-9a-fA-F]*)
               printf -v val "%b" "${val//\%/\\x}"
               val=${val%.}
               ;;
           esac
           printf -v "$var" "$val"
           ;;
      esac
    done
    set +f
} ## Updated 2013-10-12