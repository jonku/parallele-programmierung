#!/bin/bash
# ls.cgi - list recursively all files in DIRNAME=./ under KEY
# QUERY_STING="?KEY=&DIRNAME="
# 2016 Jonathan Kuhse

source ./config.cgi
source ./parse_query.cgi

unset REQUEST_METHOD  ## just in case

# Parse QUERY_STRING to variables
parse_query KEY DIRNAME

test -z "$DIRNAME" && DIRNAME="./"

cd $BUILD_DIR/$KEY || exit 2
test "${PWD##$BUILD_DIR}" != "${PWD}" || exit 4

# check if file exists
test -d "$DIRNAME" || exit 1

# check for wrong relative paths:
echo "$DIRNAME" | grep -F ".." && exit 2
echo "$DIRNAME" | grep "^/" && exit 3

echo -e "Content-Type: text/plain\n"
# ls "$DIRNAME"
find "$DIRNAME" -type f | grep -vE "(.stat|.pid|.out)"

